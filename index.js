/* globals require, module */
var request = require("superagent");
var _ = require('lodash');

(function () {
    var Topics = module.parent.require('./topics');
    var User = module.parent.require('./user');
    var url = process.env.notifyOnPostUrl;
    var username = process.env.notifyOnPostUsername;
    var password = process.env.notifyOnPostPassword;

    function hasNotifyOnPostTag(tags) {
        return _.some(tags, function (tag) {
            return tag === "notifyonpost";
        });
    }

    var plugin = {
        postSubmitted: function (data, callback) {
            callback = callback || function (err) {
                if (err) console.log("Error: ", err);
            };

            if (!url)
                return callback(new Error("No URL to send to"));

            if (!data || !data.tid || !data.uid)
                return callback(new Error("Missing required post details"));

            Topics.getTopicData(data.tid, function (err, topic) {
                if (err) return callback(err);
                if (!topic) return callback(new Error("Topic " + data.tid + " not found"));

                Topics.getTopicTags(data.tid, function (err, tags) {
                    if (err) return callback(err);

                    if (!tags || !tags.length || !hasNotifyOnPostTag(tags))
                        return callback();

                    User.getUserData(data.uid, function (err, user) {
                        if (err) return callback(err);
                        if (!user) return callback(new Error("Topic " + data.uid + " not found"));

                        request
                            .post(url)
                            .send({
                                user: {
                                    uid: user.uid,
                                    username: user.username,
                                    email: user.email,
                                    userslug: user.userslug
                                },
                                topic: {
                                    tid: topic.tid,
                                    uid: topic.uid,
                                    cid: topic.cid,
                                    title: topic.title,
                                    slug: topic.slug,
                                    tags: tags
                                }
                            })
                            .auth(username, password)
                            .end(function (err) {
                                if (err) return callback(err);

                                callback();
                            });
                    });
                });
            });
        }
    };

    module.exports = plugin;
})();